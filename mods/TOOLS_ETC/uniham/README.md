*“Unified Hammers” adds various hammers that can be used to crush nodes.*

## Default usage

The mod registers various hammers. Those hammers can be crafted from sticks and their respective material. Different material hammers can be used different times before they break.

![hammers and the needed materials](screenshots/hammers.png)

Crafting the hammers needs two sticks and two of the material as shown in the image above. Crafting is then done very similar to the other tools.

![example hammer crafting](screenshots/crafting.png)

The recipe shape is always the same, just interchange the head material. If the moreores mod is installed and loaded hammers can be crafted from the mod’s ores in the same way as shown above.

## Default functionality

The hammers added by this mod can be used to crush material into “softer” material.

![stone crushing process](screenshots/process.png)

The three default sandstone types can be crushed into the corresponding three default sand types, too.

## Durability

The durability is based on personal opinion.

| Hammer Material | Mod        | Durability (uses) |
|:----------------|:----------:|------------------:|
| Wood            | `default`  |                30 |
| Stone           | `default`  |                70 |
| Gold            | `default`  |               100 |
| Bronze          | `default`  |               150 |
| Steel           | `default`  |               200 |
| Silver          | `moreores` |               250 |
| Diamond         | `default`  |               300 |
| Mese            | `default`  |               350 |
| Obsidian        | `default`  |               400 |
| Mithril         | `moreores` |               450 |

Feel free to suggest changes.

## API usage

The *Unified Hammers API* can be used by calling the `uniham.register_hammer` function. This function takes two parameters, the ID and the definition table.

```lua
uniham.register_hammer('my_cool_id', {
    name = 'My Cool Hammer',
    head = {
        texture = 'mymod_my_cool_head_base_texture.png',
        opacity = 51.2
    }
    --- Alternative to `head`, see description below
    -- texture = 'mymod_my_cool_hammer_texture.png',
    craft = 'mymod:my_cool_item',
    uses = 200
})
```

This registers a hammer with the name “My Cool Hammer” that has the ID `mymod:my_cool_id`. The head base texture is `mymod_my_cool_texture.png` with the embossing effect being 20% visible (51.2) and the texture is automatically masked to form a hammer head.

If instead of `head` the value `texture` is provided then the automatic texture generation is disabled and the hammer will get the texture defined by the `texture` value. If both values are added then the `texture` value is given precedence.

The hammer is crafted like the provided hammers with two sticks and two of the items provided for `craft`. Alternative: When providing a [recipe table](https://dev.minetest.net/minetest.register_craft) instead of an item ID then the recipe table is used for registering the crafting recipe for the hammer (the `output` value gets automatically set by the API and can be omitted).

`uses` defines how often the hammer can be used. The value also gets stored in the tool’s definition as `_uniham.uses` that can be used by a custom `on_use` function for example

The registration table allows another, absolutely optional, parameter `on_use`. This parameter has to be a function being, well, an `on_use` function [as defined by the Minetest API](https://dev.minetest.net/register_node#on_use). This overrides the built-in `on_use` function that replaces the nodes as described. Custom `on_use` function have to handle wear and itemstack returning.
