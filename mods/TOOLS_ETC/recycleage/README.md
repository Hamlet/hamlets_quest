### ReCycleAge
![ReCycleAge's screenshot](screenshot.png)
**_Allows to recycle broken items._**

**Version:** 1.3.4  
**Source code's license:** [EUPL v1.2][1] or later.  
**Texture's license:** [CC BY-SA v3.0 Unported][2].

**Dependencies:** default, farming (found in [Minetest Game][3])  
**Compatible with:** [moreores][5]

**Not compatible with:** [toolranks][4]  
**Advanced options:** Settings -> All Settings -> Mods -> recycleage


**Description:**  
When an item breaks it will no longer vanish, instead it will break in
pieces: wooden sticks from the handle, and metal scraps or mineral shards
for mese and diamond.  
Metal scraps and mineral shards can then be turned into mese or diamonds
by cooking them into a furnace.

__Note:__  
Wooden sticks will replace the broken tool into your inventory, while
scrap metals or shards will be dropped where your character is standing.

**API**:  
You can add your own tools by using the [API](api.txt).


### Installation

Unzip the archive, rename the folder to recycleage and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods



[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://creativecommons.org/licenses/by-sa/3.0/
[3]: https://github.com/minetest/minetest_game
[4]: https://forum.minetest.net/viewtopic.php?t=18056
[5]: https://forum.minetest.net/viewtopic.php?t=549
