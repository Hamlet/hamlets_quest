--[[

	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Default scrap materials for Minetest Game v5.x
--

minetest.register_craftitem('recycleage:scrap_metal_bronze', {
	description = recycleage.l10n('Bronze scraps'),
	inventory_image = 'scrap_metal.png^[colorize:#CD7F32:255',
})

minetest.register_craftitem('recycleage:scrap_metal_steel', {
	description = recycleage.l10n('Steel scraps'),
	inventory_image = 'scrap_metal.png'
})

minetest.register_craftitem('recycleage:scrap_metal_gold', {
	description = recycleage.l10n('Gold scraps'),
	inventory_image = 'scrap_metal.png^[colorize:#cfb926:255'
})

minetest.register_craftitem('recycleage:scrap_shards_mese', {
	description = recycleage.l10n('Mese shards'),
	inventory_image = 'scrap_metal.png^[colorize:yellow:255'
})

minetest.register_craftitem('recycleage:scrap_shards_diamond', {
	description = recycleage.l10n('Diamond shards'),
	inventory_image = 'scrap_metal.png^[colorize:cyan:255'
})

minetest.register_craftitem('recycleage:scrap_shards_obsidian', {
	description = recycleage.l10n('Obsidian shards'),
	inventory_image = 'scrap_metal.png^[colorize:#1f2631:255'
})
