--[[

	Copyright © 2019, 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Scrap materials for More Ores
--

if (minetest.get_modpath('moreores') ~= nil) then

	minetest.register_craftitem('recycleage:scrap_metal_mithril', {
		description = recycleage.l10n('Mithril scraps'),
		inventory_image = 'scrap_metal.png^[colorize:#5265c5:255'
	})

	minetest.register_craftitem('recycleage:scrap_metal_silver', {
		description = recycleage.l10n('Silver scraps'),
		inventory_image = 'scrap_metal.png^[colorize:#aab5bb:255'
	})
end
