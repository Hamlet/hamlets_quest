df_primordial_items = {}
df_primordial_items.doc = {}

local MP = minetest.get_modpath(minetest.get_current_modname())

dofile(MP.."/doc.lua")
dofile(MP.."/jungle_nodes.lua")
dofile(MP.."/jungle_tree.lua")
dofile(MP.."/jungle_mushroom.lua")
dofile(MP.."/giant_fern.lua")
dofile(MP.."/fungal_nodes.lua")
dofile(MP.."/ceiling_fungus.lua")
dofile(MP.."/primordial_mushroom.lua")
dofile(MP.."/giant_mycelium.lua")