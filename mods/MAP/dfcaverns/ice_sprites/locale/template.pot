# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-12-24 16:51-0700\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ice_sprites\init.lua:11
msgid ""
"Ice sprites are mysterious glowing insect-like creatures that appear to be "
"made partly of crystalized water."
msgstr ""

#: ice_sprites\init.lua:14
msgid ""
"Ice sprites can be caught with nets and placed in bottles as sources of "
"light and freezing cold."
msgstr ""

#: ice_sprites\init.lua:16
msgid "A bottle containing a captured ice sprite."
msgstr ""

#: ice_sprites\init.lua:17
msgid "Ice sprites radiate both light and freezing cold."
msgstr ""

#: ice_sprites\init.lua:22
msgid "Ice Sprite"
msgstr ""

#: ice_sprites\init.lua:71
msgid "Hidden Ice Sprite"
msgstr ""

#: ice_sprites\init.lua:109
msgid "Ice Sprite in a Bottle"
msgstr ""
