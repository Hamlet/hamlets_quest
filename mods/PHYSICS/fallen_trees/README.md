### FALLEN TREES
**_Helps preventing the "floating treetops effect".._**

**Version:** 1.3.0  
**Source code's license:**  [EUPL v1.2][1] or later.

**Dependencies:** default (found in [Minetest Game][2])


Question: "Why have you made this mod?"  
Answer: "Because I hate floating treetops."

Question: "Why do you hate floating treetops?"  
Answer: "Because they are immersion-breaking."

Question: "What do you think about servers with forests of floating treetops?"  
Answer: "No comment."


### Installation

Unzip the archive, rename the folder to fallen_trees and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods


[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://github.com/minetest/minetest_game
