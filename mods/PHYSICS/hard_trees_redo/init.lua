--[[
	Hard Trees Redo - Prevents digging trees by punching them.
	Copyright © 2018, 2019 Hamlet <hamlatmesehub@riseup.net> and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- General variables
--

local mod_path = minetest.get_modpath("hard_trees_redo")
local S		-- Used for localization.
local NS	-- Used for localization.


--
-- Load support for intllib or Minetest Engine's localization.
--

if minetest.get_modpath("intllib") then
	S, NS = dofile(mod_path .. "/intllib.lua")

else
	S = minetest.get_translator("hard_trees_redo")

end


--
-- Rarity constants
--

local COMMON	= 5		-- drop chance: 20%
local AVERAGE	= 6.665	-- drop chance: 15%
local SCARCE	= 10	-- drop chance: 10%


--
-- Goups to be assigned
--

local bushes_groups = {
	groups = {choppy = 2, flammable = 2}
}

-- Applies to acacia, jungle tree, tree
local tree_groups_a = {
	groups = {
		tree = 1, choppy = 2, flammable = 2
	}
}

-- Applies to aspen and pine
local tree_groups_b = {
	groups = {
		tree = 1, choppy = 3, flammable = 3,
	}
}

if minetest.get_modpath("fallen_trees") then

	bushes_groups = {
		groups = {choppy = 2, flammable = 2, falling_node = 1}
	}

	tree_groups_a = {
		groups = {
			tree = 1, choppy = 2, flammable = 2, falling_node = 1
		}
	}

	tree_groups_b = {
		groups = {
			tree = 1, choppy = 3, flammable = 3, falling_node = 1
		}
	}

end


--
-- Overriden bush nodes
--

local bushes_nodes = {
	"default:bush_stem",
	"default:acacia_bush_stem",
	"default:pine_bush_stem",
}


--
-- Overriden tree nodes
--

local tree_nodes_a = {
	"default:acacia_tree", "default:jungletree", "default:tree"
}

local tree_nodes_b = {"default:aspen_tree", "default:pine_tree"}


--
-- Wooden nodes groups' overriders
--

for n = 1, 2 do
	minetest.override_item(bushes_nodes[n], bushes_groups)
end

for n = 1, 3 do
	minetest.override_item(tree_nodes_a[n], tree_groups_a)
end

for n = 1, 2 do
	minetest.override_item(tree_nodes_b[n], tree_groups_b)
end


--
-- Leaves nodes drops' overriders
--

minetest.override_item("default:acacia_bush_leaves", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:acacia_bush_sapling"}, rarity = 5},
			{items = {"default:acacia_bush_leaves"}},
			{items = {"default:stick"}, rarity = SCARCE}
		}
	}
})

minetest.override_item("default:acacia_leaves", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:acacia_sapling"}, rarity = 20},
			{items = {"default:acacia_leaves"}},
			{items = {"default:stick"}, rarity = SCARCE}
		}
	}
})

minetest.override_item("default:aspen_leaves", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:aspen_sapling"}, rarity = 20},
			{items = {"default:aspen_leaves"}},
			{items = {"default:stick"}, rarity = AVERAGE}
		}
	}
})


minetest.override_item("default:bush_leaves", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:bush_sapling"}, rarity = 5},
			{items = {"default:bush_leaves"}},
			{items = {"default:stick"}, rarity = AVERAGE}
		}
	}
})

minetest.override_item("default:jungleleaves", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:junglesapling"}, rarity = 20},
			{items = {"default:jungleleaves"}},
			{items = {"default:stick"}, rarity = COMMON}
		}
	}
})

minetest.override_item("default:pine_needles", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:pine_sapling"}, rarity = 20},
			{items = {"default:pine_needles"}},
			{items = {"default:stick"}, rarity = SCARCE}
		}
	}
})


minetest.override_item("default:pine_bush_needles", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:pine_sapling"}, rarity = 20},
			{items = {"default:pine_needles"}},
			{items = {"default:stick"}, rarity = SCARCE}
		}
	}
})


minetest.override_item("default:leaves", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:sapling"}, rarity = 20},
			{items = {"default:leaves"}},
			{items = {"default:stick"}, rarity = AVERAGE}
		}
	}
})


--
-- Dirt nodes drops' overriders
--

if (minetest.get_modpath("bonemeal") == nil) then
	minetest.override_item("default:dirt", {
		drop = {
			max_items = 2,
			items = {
				{items = {"default:dirt"}},
				{items = {"hard_trees_redo:rock"}, rarity = AVERAGE}
			}
		}
	})

else
	minetest.override_item("default:dirt", {
		drop = {
			max_items = 3,
			items = {
				{items = {"default:dirt"}},
				{items = {"hard_trees_redo:rock"}, rarity = AVERAGE},
				{items = {"bonemeal:bone"},	rarity = 30},
			}
		}
	})
end

minetest.override_item("default:dirt_with_grass", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:dirt"}},
			{items = {"hard_trees_redo:rock"}, rarity = AVERAGE}
		}
	}
})

minetest.override_item("default:dirt_with_grass_footsteps", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:dirt"}},
			{items = {"hard_trees_redo:rock"}, rarity = AVERAGE}
		}
	}
})

minetest.override_item("default:dirt_with_dry_grass", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:dirt"}},
			{items = {"hard_trees_redo:rock"}, rarity = COMMON}
		}
	}
})

minetest.override_item("default:dirt_with_snow", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:dirt"}},
			{items = {"hard_trees_redo:rock"}, rarity = COMMON}
		}
	}
})

minetest.override_item("default:dirt_with_rainforest_litter", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:dirt"}},
			{items = {"hard_trees_redo:rock"}, rarity = SCARCE}
		}
	}
})


minetest.override_item("default:dirt_with_coniferous_litter", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:dirt"}},
			{items = {"hard_trees_redo:rock"}, rarity = COMMON}
		}
	}
})

minetest.override_item("default:dry_dirt", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:dry_dirt"}},
			{items = {"hard_trees_redo:rock"}, rarity = AVERAGE}
		}
	}
})

minetest.override_item("default:dry_dirt_with_dry_grass", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:dry_dirt"}},
			{items = {"hard_trees_redo:rock"}, rarity = COMMON}
		}
	}
})

minetest.override_item("default:permafrost", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:permafrost"}},
			{items = {"hard_trees_redo:rock"}, rarity = SCARCE}
		}
	}
})

minetest.override_item("default:permafrost_with_stones", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:permafrost_with_stones"}},
			{items = {"hard_trees_redo:rock"}, rarity = COMMON}
		}
	}
})

minetest.override_item("default:permafrost_with_moss", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:permafrost_with_moss"}},
			{items = {"hard_trees_redo:rock"}, rarity = AVERAGE}
		}
	}
})



--
-- Stone nodes drops' overriders
--

minetest.override_item("default:stone", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:cobble"}},
			{items = {"hard_trees_redo:rock"}, rarity = COMMON}
		}
	}
})

minetest.override_item("default:cobble", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:cobble"}},
			{items = {"hard_trees_redo:rock"}, rarity = AVERAGE}
		}
	}
})

minetest.override_item("default:mossycobble", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:mossycobble"}},
			{items = {"hard_trees_redo:rock"}, rarity = AVERAGE}
		}
	}
})

minetest.override_item("default:desert_stone", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:desert_cobble"}},
			{items = {"hard_trees_redo:rock_desert"}, rarity = COMMON}
		}
	}
})

minetest.override_item("default:desert_cobble", {
	drop = {
		max_items = 2,
		items = {
			{items = {"default:desert_cobble"}},
			{items = {"hard_trees_redo:rock_desert"}, rarity = AVERAGE}
		}
	}
})


--
-- Nodes reassigned to the "rocks" group
--

minetest.override_item("default:flint", {
	groups = {rocks = 1}
})


--
-- Wooden tools' removers
--

minetest.unregister_item("default:axe_wood")
minetest.clear_craft{output = "default:axe_wood"}

minetest.unregister_item("default:pick_wood")
minetest.clear_craft{output = "default:pick_wood"}


--
-- Additional items and tools recipes" (re)definitions
--

minetest.register_craftitem("hard_trees_redo:rock", {
	description = S("Rock"),
	inventory_image = "hard_trees_redo_rock.png",
	groups = {rocks = 1},
})

minetest.register_craftitem("hard_trees_redo:rock_desert", {
	description = S("Desert Rock"),
	inventory_image = "hard_trees_redo_rock_desert.png",
	groups = {rocks = 1},
})

-- Aliases for backward compatibility

minetest.register_alias("ht_redo:rock", "hard_trees_redo:rock")
minetest.register_alias("ht_redo:rock_desert",
	"hard_trees_redo:rock_desert")


minetest.register_craft({
	output = "default:cobble",
	recipe = {
		{
			"hard_trees_redo:rock",
			"hard_trees_redo:rock",
			"hard_trees_redo:rock"
		},
		{
			"hard_trees_redo:rock",
			"hard_trees_redo:rock",
			"hard_trees_redo:rock"
		},
		{
			"hard_trees_redo:rock",
			"hard_trees_redo:rock",
			"hard_trees_redo:rock"
		},
	}
})

minetest.register_craft({
	output = "default:desert_cobble",
	recipe = {
		{
			"hard_trees_redo:rock_desert",
			"hard_trees_redo:rock_desert",
			"hard_trees_redo:rock_desert"
		},
		{
			"hard_trees_redo:rock_desert",
			"hard_trees_redo:rock_desert",
			"hard_trees_redo:rock_desert"
		},
		{
			"hard_trees_redo:rock_desert",
			"hard_trees_redo:rock_desert",
			"hard_trees_redo:rock_desert"
		},
	}
})


minetest.clear_craft({output = "default:axe_stone"})
minetest.register_craft({
	output = "default:axe_stone",
	recipe = {
		{
			"group:rocks", "group:rocks"
		},
		{
			"group:rocks", "group:stick"
		},
		{
			""			  , "group:stick"
		},
	}
})


minetest.clear_craft({output = "default:pick_stone"})
minetest.register_craft({
	output = "default:pick_stone",
	recipe = {
		{
			"group:rocks", "group:rocks", "group:rocks"
		},
		{
			"",            "group:stick", ""
		},
		{
			"",            "group:stick", ""
		},
	}
})


minetest.clear_craft({output = "default:shovel_stone"})
minetest.register_craft({
	output = "default:shovel_stone",
	recipe = {
		{
			"group:rocks"
		},
		{
			"group:stick"
		},
		{
			"group:stick"
		},
	}
})


minetest.clear_craft({output = "default:sword_stone"})
minetest.register_craft({
	output = "default:sword_stone",
	recipe = {
		{
			"group:rocks"
		},
		{
			"group:rocks"
		},
		{
			"group:stick"
		},
	}
})


minetest.clear_craft({output = "farming:hoe_stone"})
minetest.register_craft({
	output = "farming:hoe_stone",
	recipe = {
		{
			"group:rocks", "group:rocks"
		},
		{
			""           , "group:stick"
		},
		{
			""           , "group:stick"
		},
	}
})


--
-- Support for [Mod] Real Trees [real_trees]
--

if minetest.get_modpath("real_trees") then
	dofile(mod_path .. "/real_trees.lua")
end


--
-- Support for [Mod] Desert Life [desert_life]
--

if minetest.get_modpath("desert_life") then
	dofile(mod_path .. "/desert_life.lua")
end


--
-- Flush the no longer used variables, constants and tables
--

mod_path = nil
S = nil
NS = nil
COMMON = nil
AVERAGE = nil
SCARCE = nil
bushes_nodes = nil
tree_nodes_a = nil
tree_nodes_b = nil
bushes_groups = nil
tree_groups_a = nil
tree_groups_b = nil


--
-- Minetest engine debug logging
--

local log_level = minetest.settings:get("debug_log_level")

if (log_level == nil)
or (log_level == "action")
or (log_level == "info")
or (log_level == "verbose")
then
	log_level = nil
	minetest.log("action", "[Mod] Hard Trees Redo [v0.2.0] loaded.")
end
