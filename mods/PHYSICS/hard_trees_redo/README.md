### Hard Trees Redo
**_Prevents digging trees by punching them._**

**Source code's license:** [EUPL v1.2][1] or later.  
**Textures' license:** [CC BY-SA v4.0][2] or later.

**Dependencies:** default, farming (found in [Minetest Game][3])  
**Supported:** [bonemeal][4], [intllib][5], [fallen_nodes][6], [fallen_trees][7]


__Advanced option:__  
Settings -> All settings -> Mods -> hard_trees_redo


**Description:**  
Leaf nodes will drop sticks, dirt/cobble/stone/etc. nodes will drop rocks.  
Wooden axe and wooden pick will be removed.  
Stone tools can be crafted using sticks and rocks (or flints).  
Rocks can be used to craft cobblestone nodes, recipe:  

R = Rock

RRR  
RRR  
RRR

9 rocks give 1 cobblestone node.

These game mechanics have been ported from [Voxelands][8].



### Installation

Unzip the archive, rename the folder to hard_trees_redo and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods



### Changelog

[Change Log.md](/changelog.md/)



[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://creativecommons.org/licenses/by-sa/4.0/
[3]: https://github.com/minetest/minetest_game
[4]: https://forum.minetest.net/viewtopic.php?t=16446
[5]: https://forum.minetest.net/viewtopic.php?t=4929
[6]: https://forum.minetest.net/viewtopic.php?t=18239
[7]: https://forum.minetest.net/viewtopic.php?t=18252
[8]: https://gitlab.com/voxelands/voxelands
