### FALLEN NODES
**_Adds dirt, cobble, straw and cactus nodes to the falling_node group. Papyrus will fall too._**

**Version:** 1.5.0
**License:** [EUPL v1.2][1] or later.  

**Dependencies:** default, stairs, farming (found [Minetest Game][2])  
**Supported:** [Landscape][3], [Experimental Mapgen][4], [Darkage][5] (Addi's fork)

**Advanced setting:** Settings -> All Settings -> Mods -> fallen_nodes

**Description:**  
Dirt, cobble, straw and cactus nodes nodes will no longer 'float'.
Papyrus nodes will fall as well.

**Credits**:  
Thanks to TenPlus1 for [this post][6], which allowed me to greatly improve the code.


### Installation

Unzip the archive, rename the folder to fallen_nodes and place it in  
../minetest/mods/

If you only want this to be used in a single world, place it in  
../minetest/worlds/WORLD_NAME/worldmods/

GNU+Linux - If you use a system-wide installation place it in  
~/.minetest/mods/

For further information or help see:  
https://wiki.minetest.net/Help:Installing_Mods



[1]: https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863
[2]: https://github.com/minetest/minetest_game
[3]: https://forum.minetest.net/viewtopic.php?t=5190
[4]: https://forum.minetest.net/viewtopic.php?t=7263
[5]: https://forum.minetest.net/viewtopic.php?t=3213
[6]: https://forum.minetest.net/viewtopic.php?p=361200#p361200
