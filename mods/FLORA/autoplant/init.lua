local parent_add_item = minetest.add_item
function minetest.add_item(pos, name)
  -- quick-n-dirty solution, but obviosly may produce a bug
  if type(name) == 'string' and name:match('sapling$') then
    local i, node_under = 0, nil
    repeat
      i = i + 1
      node_under = minetest.get_node({ x = pos.x, y = pos.y - i, z = pos.z })
    until i > 50 or
          (node_under.name ~= 'air' and
           minetest.get_item_group(node_under.name, 'leafdecay') == 0)
    if minetest.get_item_group(node_under.name, 'soil') > 0 then
      local target = { x = pos.x, y = pos.y - i + 1, z = pos.z }
      if minetest.get_node(target).name == 'air' then
        return minetest.set_node(target, { name = name })
      end
    end
  end
  return parent_add_item(pos, name)
end
