--[[

	Archer's tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Disable the stone bow

minetest.clear_craft({output = 'archer:bow_stone'})
minetest.unregister_item('archer:bow_stone')


-- Redefine the other bows' crafting recipes

minetest.clear_craft({output = 'archer:bow_wood'})
minetest.clear_craft({output = 'archer:bow_steel'})
minetest.clear_craft({output = 'archer:bow_bronze'})
minetest.clear_craft({output = 'archer:bow_mese'})
minetest.clear_craft({output = 'archer:bow_diamond'})

minetest.register_craft({
	output = 'archer:bow_wood',
	recipe = {
		{'farming:string', 'group:wood', ''},
		{'farming:string', '', 'group:wood'},
		{'farming:string', 'group:wood', ''},
	}
})

minetest.register_craft({
	output = 'archer:bow_steel',
	recipe = {
		{'farming:string', 'default:steel_ingot', ''},
		{'farming:string', '', 'default:steel_ingot'},
		{'farming:string', 'default:steel_ingot', ''},
	}
})

minetest.register_craft({
	output = 'archer:bow_bronze',
	recipe = {
		{'farming:string', 'default:bronze_ingot', ''},
		{'farming:string', '', 'default:bronze_ingot'},
		{'farming:string', 'default:bronze_ingot', ''},
	}
})

minetest.register_craft({
	output = 'archer:bow_mese',
	recipe = {
		{'farming:string', 'default:mese_crystal', ''},
		{'farming:string', '', 'default:mese_crystal'},
		{'farming:string', 'default:mese_crystal', ''},
	}
})

minetest.register_craft({
	output = 'archer:bow_diamond',
	recipe = {
		{'farming:string', 'default:diamond', ''},
		{'farming:string', '', 'default:diamond'},
		{'farming:string', 'default:diamond', ''},
	}
})


-- Allow to recycle broken bows
--[[
recycleage.after_use_overrider('archer:bow_wood',
	'default:wood', 2, 'farming:string', 2)

recycleage.after_use_overrider('archer:bow_steel',
	'recycleage:scrap_metal_steel', 2, 'farming:string', 2)

recycleage.after_use_overrider('archer:bow_bronze',
	'recycleage:scrap_metal_bronze', 2, 'farming:string', 2)

recycleage.after_use_overrider('archer:bow_mese',
	'recycleage:scrap_shards_mese', 2, 'farming:string', 2)

recycleage.after_use_overrider('archer:bow_diamond',
	'recycleage:scrap_shards_diamond', 2, 'farming:string', 2)
--]]
