--[[

	Real Minerals' tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Allow converting certain ingots to default ingots and vice-versa

-- Real Minerals to default

minetest.register_craft({
	output = "default:bronze_ingot",
	recipe = {
		{"real_minerals:bronze_ingot"},
	}
})

minetest.register_craft({
	output = "default:copper_ingot",
	recipe = {
		{"real_minerals:copper_ingot"},
	}
})

minetest.register_craft({
	output = "default:gold_ingot",
	recipe = {
		{"real_minerals:gold_ingot"},
	}
})

minetest.register_craft({
	output = "default:steel_ingot",
	recipe = {
		{"real_minerals:black_steel_ingot"},
	}
})

minetest.register_craft({
	output = "default:steel_ingot",
	recipe = {
		{"real_minerals:steel_ingot"},
	}
})

minetest.register_craft({
	output = "default:tin_ingot",
	recipe = {
		{"real_minerals:tin_ingot"},
	}
})

-- Default to Real Minerals

minetest.register_craft({
	output = "real_minerals:bronze_ingot",
	recipe = {
		{"default:bronze_ingot"},
	}
})

minetest.register_craft({
	output = "real_minerals:copper_ingot",
	recipe = {
		{"default:copper_ingot"},
	}
})

minetest.register_craft({
	output = "real_minerals:gold_ingot",
	recipe = {
		{"default:gold_ingot"},
	}
})

minetest.register_craft({
	output = "real_minerals:steel_ingot",
	recipe = {
		{"default:steel_ingot"},
	}
})

minetest.register_craft({
	output = "real_minerals:tin_ingot",
	recipe = {
		{"default:tin_ingot"},
	}
})

-- Allow to revert metal blocks to ingots

minetest.register_craft({
	output = "real_minerals:aluminium_ingot 9",
	recipe = {
		{"real_minerals:aluminium_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:bismuth_ingot 9",
	recipe = {
		{"real_minerals:bismuth_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:bronze_ingot 9",
	recipe = {
		{"real_minerals:bronze_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:copper_ingot 9",
	recipe = {
		{"real_minerals:copper_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:gold_ingot 9",
	recipe = {
		{"real_minerals:gold_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:lead_ingot 9",
	recipe = {
		{"real_minerals:lead_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:nickel_ingot 9",
	recipe = {
		{"real_minerals:nickel_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:pig_iron_ingot 9",
	recipe = {
		{"real_minerals:pig_iron_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:platinum_ingot 9",
	recipe = {
		{"real_minerals:platinum_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:silver_ingot 9",
	recipe = {
		{"real_minerals:silver_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:steel_ingot 9",
	recipe = {
		{"real_minerals:steel_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:tin_ingot 9",
	recipe = {
		{"real_minerals:tin_block"},
	}
})

minetest.register_craft({
	output = "real_minerals:zinc_ingot 9",
	recipe = {
		{"real_minerals:zinc_block"},
	}
})

-- Allow to craft torches using anthracite

minetest.register_craft({
	output = "default:torch 5",
	recipe = {
		{"real_minerals:anthracite"},
		{"group:stick"}
	}
})
