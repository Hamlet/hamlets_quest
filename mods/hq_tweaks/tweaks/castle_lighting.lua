--[[

	Castle Lighting's tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Make the light block's recipe more expensive

minetest.clear_craft({output = "castle_lighting:light"})

minetest.register_craft({
	output = "castle_lighting:light",
	recipe = {
		{"default:stick", "default:glass", "default:stick"},
		{"default:glass", "default:mese_crystal", "default:glass"},
		{"default:stick", "default:glass", "default:stick"},
	}
})


-- Make the chandelier's recipe more expensive

minetest.clear_craft({output = "castle_lighting:chandelier"})

minetest.register_craft({
	output = "castle_lighting:chandelier",
	recipe = {
		{"", "", ""},
		{"", "default:steel_ingot", ""},
		{"default:mese_crystal_fragment",
		 "default:mese_crystal_fragment",
		 "default:mese_crystal_fragment"},
	}
})

-- change chain and chandelier's models and textures
-- these are by VanessaE from Homedecor - Chains (CC0)

minetest.override_item("castle_lighting:chandelier_chain", {
	drawtype = "mesh",
	mesh = "chains.obj",
	tiles = {"chains_wrought_iron.png"},
	walkable = false,
	climbable = true,
	sunlight_propagates = true,
	paramtype = "light",
	inventory_image = "chain_wrought_iron_inv.png",
	groups = {cracky=2},
	selection_box =  {
		type = "fixed",
			fixed = {
				{ -0.1, -0.625, -0.1, 0.1, 0.5, 0.1 }
			},
		},
	sounds = default.node_sound_glass_defaults(),
})

minetest.override_item("castle_lighting:chandelier", {
	drawtype = "mesh",
	mesh = "chains_chandelier.obj",
	wield_image = "",
	inventory_image = "",
	paramtype = "light",
	sunlight_propagates = true,
	groups = {cracky=2},
	light_source = 14,
	tiles = {
		"chains_wrought_iron.png",
		"chains_candle.png",
		{
			name="chains_candle_flame.png",
			animation={
				type="vertical_frames",
				aspect_w=16,
				aspect_h=16,
				length=3.0
			}
		}
	},

	sounds = default.node_sound_glass_defaults(),
})
