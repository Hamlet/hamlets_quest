--[[

	PEAK Simple Arcs - with doors's tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Unregister bugged nodes
local t_PKARCS = {
	'pkarcs:cobble_arc',
	'pkarcs:cobble_inner_arc',
	'pkarcs:cobble_outer_arc',
	'pkarcs:mossycobble_arc',
	'pkarcs:mossycobble_inner_arc',
	'pkarcs:mossycobble_outer_arc'
}

for i_Item = 1, #t_PKARCS do
	minetest.clear_craft({output = t_PKARCS[i_Item]})
	minetest.unregister_item(t_PKARCS[i_Item])
end

--[[
-- Register a one-time run LBM to replace the in-world bugged nodes
minetest.register_lbm({
	label = 'HQTweaks - Bugged PKArcs remover LBM',
	-- Descriptive label for profiling purposes (optional).
	-- Definitions with identical labels will be listed as one.

	name = ':hqtweaks_bugged_arcs_remover',

	nodenames = t_PKARCS,
	-- List of node names to trigger the LBM on.
	-- Also non-registered nodes will work.
	-- Groups (as of group:groupname) will work as well.

	run_at_every_load = true,
	-- Whether to run the LBM's action every time a block gets loaded,
	-- and not only the first time the block gets loaded after the LBM
	-- was introduced.

	action = function(pos, node)
		if (node.name == 'pkarcs:cobble_arc')
		or (node.name == 'pkarcs:mossycobble_arc')
		then
			node.name = 'pkarcs:stone_arc'
			minetest.swap_node(pos, node)
		end

		if (node.name == 'pkarcs:cobble_inner_arc')
		or (node.name == 'pkarcs:mossycobble_inner_arc')
		then
			node.name = 'pkarcs:stone_inner_arc'
			minetest.swap_node(pos, node)
		end

		if (node.name == 'pkarcs:cobble_outer_arc')
		or (node.name == 'pkarcs:mossycobble_outer_arc')
		then
			node.name = 'pkarcs:stone_outer_arc'
			minetest.swap_node(pos, node)
		end
	end
})
--]]

-- Nodes to be registered
local t_CASTLE_MASONRY = {
	'castle_masonry:stonewall',
	'castle_masonry:stonewall_corner',
	'castle_masonry:pavement_brick',
	'castle_masonry:dungeon_stone'
}

local t_DARKAGE = {
	'darkage:adobe',
	'darkage:basalt',
	'darkage:basalt_block',
	'darkage:basalt_brick',
	'darkage:chalked_bricks',
	'darkage:gneiss',
	'darkage:gneiss_block',
	'darkage:gneiss_brick',
	'darkage:marble',
	'darkage:marble_tile',
	'darkage:old_tuff_bricks',
	'darkage:ors',
	'darkage:ors_block',
	'darkage:ors_brick',
	'darkage:rhyolitic_tuff',
	'darkage:rhyolitic_tuff_bricks',
	'darkage:schist',
	'darkage:slate',
	'darkage:slate_block',
	'darkage:slate_brick',
	'darkage:slate_tile',
	'darkage:stone_brick',
	'darkage:tuff',
	'darkage:tuff_bricks'
}


-- Register Castle Masonry's nodes
for i_Item = 1, #t_CASTLE_MASONRY do
	pkarcs.register_node(t_CASTLE_MASONRY[i_Item])
end

-- Register Darkage's nodes
for i_Item = 1, #t_DARKAGE do
	pkarcs.register_node(t_DARKAGE[i_Item])
end

-- Flush the tables for memory saving
t_PKARCS = nil
t_CASTLE_MASONRY = nil
t_DARKAGE = nil
