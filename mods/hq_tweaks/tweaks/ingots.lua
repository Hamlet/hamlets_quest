--[[

	Ingots' tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Allow to place Real Minerals' ingots

local t_RealMineralsIngots = {
	{'real_minerals:bismuth_ingot',
	'ingot_silver.png^[colorize:#3c6da7:127'},

	{'real_minerals:zinc_ingot',
	'ingot_silver.png^[colorize:#c9c8c9:127'},

	{'real_minerals:tin_ingot',
	'ingot_silver.png^[colorize:#c1c6cc:127'},

	{'real_minerals:copper_ingot',
	'ingot_silver.png^[colorize:#df5c38:127'},

	{'real_minerals:lead_ingot',
	'ingot_silver.png^[colorize:#5c606d:127'},

	{'real_minerals:silver_ingot',
	'ingot_silver.png^[colorize:#aeaeae:127'},

	{'real_minerals:gold_ingot',
	'ingot_silver.png^[colorize:#aaaa07:127'},

	{'real_minerals:brass_ingot',
	'ingot_silver.png^[colorize:#ac9f3b:127'},

	{'real_minerals:sterling_silver_ingot',
	'ingot_silver.png^[colorize:#877260:127'},

	{'real_minerals:rose_gold_ingot',
	'ingot_silver.png^[colorize:#c48759:127'},

	{'real_minerals:oroide_ingot',
	'ingot_silver.png^[colorize:#e3d1aa:127'},

	{'real_minerals:black_bronze_ingot',
	'ingot_silver.png^[colorize:#2e1f28:127'},

	{'real_minerals:bismuth_bronze_ingot',
	'ingot_silver.png^[colorize:#2a5933:127'},

	{'real_minerals:tumbaga_ingot',
	'ingot_silver.png^[colorize:#e7b100:127'},

	{'real_minerals:bronze_ingot',
	'ingot_silver.png^[colorize:#8e6e2f:127'},

	{'real_minerals:aluminium_ingot',
	'ingot_silver.png^[colorize:#cccccc:127'},

	{'real_minerals:platinum_ingot',
	'ingot_silver.png^[colorize:#768291:127'},

	{'real_minerals:pig_iron_ingot',
	'ingot_silver.png^[colorize:#847376:127'},

	{'real_minerals:wrought_iron_ingot',
	'ingot_silver.png^[colorize:#999999:127'},

	{'real_minerals:german_silver_ingot',
	'ingot_silver.png^[colorize:#929aa0:127'},

	{'real_minerals:albata_ingot',
	'ingot_silver.png^[colorize:#8ea49f:127'},

	{'real_minerals:nickel_ingot',
	'ingot_silver.png^[colorize:#6e6e57:127'},

	{'real_minerals:steel_ingot',
	'ingot_silver.png^[colorize:#8c8c9b:127'},

	{'real_minerals:monel_ingot',
	'ingot_silver.png^[colorize:#9aa191:127'},

	{'real_minerals:black_steel_ingot',
	'ingot_silver.png^[colorize:#2a2f2f:127'}
}


-- Register the placeable ingots
for  _, s_Value in ipairs(t_RealMineralsIngots) do
	ingots.register_ingots(s_Value[1], s_Value[2], false)
end

-- Flush the table for memory saving
t_RealMineralsIngots = nil
