--[[

	Thirsty's tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Change the bowl's name to make it clear that it's for drinking only.
minetest.override_item("thirsty:wooden_bowl",
	{description = "Wooden Bowl (drink only)"})

-- Redefine the wooden bowl's recipe to avoid conflicts with Farming Redo's.

minetest.clear_craft({output = 'thirsty:wooden_bowl'})

minetest.register_craft({
	output = 'thirsty:wooden_bowl',
	type = 'shapeless',
	recipe = {'stairs:slab_wood'}
})

minetest.register_craft({
	output = 'thirsty:wooden_bowl',
	type = 'shapeless',
	recipe = {'stairs:slab_pine_wood'}
})

minetest.register_craft({
	output = 'thirsty:wooden_bowl',
	type = 'shapeless',
	recipe = {'stairs:slab_aspen_wood'}
})

minetest.register_craft({
	output = 'thirsty:wooden_bowl',
	type = 'shapeless',
	recipe = {'stairs:slab_acacia_wood'}
})

minetest.register_craft({
	output = 'thirsty:wooden_bowl',
	type = 'shapeless',
	recipe = {'stairs:slab_junglewood'}
})
