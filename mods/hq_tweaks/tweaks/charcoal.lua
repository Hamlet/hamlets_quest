--[[

	Charcoal's tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Allow to craft a charcoal block.
-- Source code taken from Minetest Game

minetest.register_node("hq_tweaks:charcoal_block", {
	description = "Charcoal Block",
	tiles = {"hq_tweaks_charcoal_block.png"},
	is_ground_content = false,
	groups = {cracky = 3},
	sounds = default.node_sound_stone_defaults(),
})

minetest.register_craft({
	output = "hq_tweaks:charcoal_block",
	recipe = {
		{"charcoal:charcoal_lump", "charcoal:charcoal_lump",
			"charcoal:charcoal_lump"},
		{"charcoal:charcoal_lump", "charcoal:charcoal_lump",
			"charcoal:charcoal_lump"},
		{"charcoal:charcoal_lump", "charcoal:charcoal_lump",
			"charcoal:charcoal_lump"},
	}
})

minetest.register_craft({
	type = "fuel",
	recipe = "hq_tweaks:charcoal_block",
	burntime = 370,
})

minetest.register_craft({
	output = "charcoal:charcoal_lump 9",
	recipe = {
		{"hq_tweaks:charcoal_block"},
	}
})
