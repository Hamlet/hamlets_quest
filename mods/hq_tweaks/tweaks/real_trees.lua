--[[

	Real Trees' tweaks
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


-- Allow to walk through and to climb on leaves.

local t_RealTreesLeaves = {

	-- Acacia tree
	"real_trees:small_acacia_leaves",
	"real_trees:corner_acacia_leaves",
	"real_trees:wide_acacia_leaves",
	"real_trees:acacia_leaf_slab",

	-- Jungletree
	"real_trees:small_jungle_leaves",
	"real_trees:corner_jungle_leaves",
	"real_trees:wide_jungle_leaves",
	"real_trees:jungle_leaf_slab",

	-- Tree
	"real_trees:small_leaves",
	"real_trees:corner_leaves",
	"real_trees:wide_leaves",
	"real_trees:leaf_slab",

	-- Aspen tree
	"real_trees:small_aspen_leaves",
	--"real_trees:u_small_aspen_leaves",
	"real_trees:corner_aspen_leaves",
	"real_trees:wide_aspen_leaves",
	"real_trees:aspen_leaf_slab",

	-- Pine tree
	"real_trees:small_pine_needles",
	"real_trees:corner_pine_needles",
	"real_trees:m_corner_pine_needles",
	"real_trees:wide_pine_needles",
	"real_trees:m_wide_pine_needles",
	"real_trees:pine_needle_slab",
	"real_trees:b_pine_needle_slab",
	"real_trees:m_pine_needle_slab"
}

for i_Element = 1, #t_RealTreesLeaves do
	soft_leaves.overrider(t_RealTreesLeaves[i_Element])
end

-- Flush the table for memory saving.
t_RealTreesLeaves = nil
