--[[

	Applies various tweaks to several mods.
	Copyright © 2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Constants
--

local s_MOD_PATH = minetest.get_modpath('hq_tweaks')
local s_LOG_LEVEL = minetest.settings:get('debug_log_level')


--
-- Tweaks' files loaders
--

dofile(s_MOD_PATH .. '/tweaks/anvil.lua')
dofile(s_MOD_PATH .. '/tweaks/archer.lua')
dofile(s_MOD_PATH .. '/tweaks/basic_materials.lua')
dofile(s_MOD_PATH .. '/tweaks/campfire.lua')
dofile(s_MOD_PATH .. '/tweaks/castle_lighting.lua')
dofile(s_MOD_PATH .. '/tweaks/castle_tapestries.lua')
dofile(s_MOD_PATH .. '/tweaks/castle_weapons.lua')
dofile(s_MOD_PATH .. '/tweaks/charcoal.lua')
dofile(s_MOD_PATH .. '/tweaks/darkage.lua')
dofile(s_MOD_PATH .. '/tweaks/dfcaverns.lua')
dofile(s_MOD_PATH .. '/tweaks/enchanting.lua')
dofile(s_MOD_PATH .. '/tweaks/farming_redo.lua')
dofile(s_MOD_PATH .. '/tweaks/gravelsieve.lua')
--dofile(s_MOD_PATH .. '/tweaks/handholds.lua')
dofile(s_MOD_PATH .. '/tweaks/hardtorch.lua')
dofile(s_MOD_PATH .. '/tweaks/ingots.lua')
dofile(s_MOD_PATH .. '/tweaks/minetest_game.lua')
dofile(s_MOD_PATH .. '/tweaks/pkarcs.lua')
dofile(s_MOD_PATH .. '/tweaks/real_minerals.lua')
dofile(s_MOD_PATH .. '/tweaks/real_trees.lua')
dofile(s_MOD_PATH .. '/tweaks/thirsty.lua')
dofile(s_MOD_PATH .. '/tweaks/unifieddyes.lua')
--dofile(s_MOD_PATH .. '/tweaks/uniham.lua')


--
-- Minetest engine debug logging
--

if (s_LOG_LEVEL == nil)
or (s_LOG_LEVEL == 'action')
or (s_LOG_LEVEL == 'info')
or (s_LOG_LEVEL == 'verbose')
then
	minetest.log('action', '[Mod] Hamlet\'s Quest\'s Tweaks [v0.1.0] loaded.')
end
