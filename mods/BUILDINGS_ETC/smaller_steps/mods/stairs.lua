--[[
	Smaller Steps - Makes stairs and slabs use smaller shapes.
	Copyright © 2018-2020 Hamlet and contributors.

	Licensed under the EUPL, Version 1.2 or – as soon they will be
	approved by the European Commission – subsequent versions of the
	EUPL (the "Licence");
	You may not use this work except in compliance with the Licence.
	You may obtain a copy of the Licence at:

	https://joinup.ec.europa.eu/software/page/eupl
	https://eur-lex.europa.eu/legal-content/EN/TXT/?uri=CELEX:32017D0863

	Unless required by applicable law or agreed to in writing,
	software distributed under the Licence is distributed on an
	"AS IS" basis,
	WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
	implied.
	See the Licence for the specific language governing permissions
	and limitations under the Licence.

--]]


--
-- Procedure
--

local pr_StairsOverriders = function()

	-- Constants
	local t_nodesStairsNormal = {
		'stairs:stair_cobble', 'stairs:stair_mossycobble',
		'stairs:stair_acacia_wood', 'stairs:stair_aspen_wood',
		'stairs:stair_brick', 'stairs:stair_bronzeblock',
		'stairs:stair_copperblock', 'stairs:stair_desert_cobble',
		'stairs:stair_desert_stone', 'stairs:stair_desert_stone_block',
		'stairs:stair_desert_stonebrick', 'stairs:stair_desert_sandstone',
		'stairs:stair_desert_sandstone_block',
		'stairs:stair_desert_sandstone_brick',
		'stairs:stair_goldblock', 'stairs:stair_ice', 'stairs:stair_obsidian',
		'stairs:stair_obsidian_block', 'stairs:stair_obsidianbrick',
		'stairs:stair_pine_wood', 'stairs:stair_sandstone',
		'stairs:stair_sandstone_block', 'stairs:stair_sandstonebrick',
		'stairs:stair_silver_sandstone', 'stairs:stair_silver_sandstone_block',
		'stairs:stair_silver_sandstone_brick', 'stairs:stair_snowblock',
		'stairs:stair_steelblock', 'stairs:stair_stone',
		'stairs:stair_stone_block', 'stairs:stair_stonebrick',
		'stairs:stair_straw', 'stairs:stair_wood', 'stairs:stair_glass',
		'stairs:stair_junglewood', 'stairs:stair_obsidian_glass',
		'stairs:stair_tinblock'
	}

	local t_nodesStairsOuter = {
		'stairs:stair_outer_cobble', 'stairs:stair_outer_mossycobble',
		'stairs:stair_outer_acacia_wood', 'stairs:stair_outer_aspen_wood',
		'stairs:stair_outer_brick', 'stairs:stair_outer_bronzeblock',
		'stairs:stair_outer_copperblock', 'stairs:stair_outer_desert_cobble',
		'stairs:stair_outer_desert_cobble',
		'stairs:stair_outer_desert_sandstone',
		'stairs:stair_outer_desert_sandstone_block',
		'stairs:stair_outer_desert_sandstone_brick',
		'stairs:stair_outer_desert_stone',
		'stairs:stair_outer_desert_stone_block',
		'stairs:stair_outer_desert_stonebrick', 'stairs:stair_outer_goldblock',
		'stairs:stair_outer_ice', 'stairs:stair_outer_obsidian',
		'stairs:stair_outer_obsidian_block', 'stairs:stair_outer_obsidianbrick',
		'stairs:stair_outer_pine_wood', 'stairs:stair_outer_sandstone',
		'stairs:stair_outer_sandstone_block',
		'stairs:stair_outer_sandstonebrick',
		'stairs:stair_outer_silver_sandstone',
		'stairs:stair_outer_silver_sandstone_block',
		'stairs:stair_outer_silver_sandstone_brick',
		'stairs:stair_outer_snowblock', 'stairs:stair_outer_steelblock',
		'stairs:stair_outer_stone', 'stairs:stair_outer_stone_block',
		'stairs:stair_outer_stonebrick', 'stairs:stair_outer_straw',
		'stairs:stair_outer_tinblock', 'stairs:stair_outer_wood',
		'stairs:stair_outer_glass', 'stairs:stair_outer_junglewood',
		'stairs:stair_outer_obsidian_glass', 'stairs:stair_outer_tinblock'
	}

	local t_nodesStairsInner = {
		'stairs:stair_inner_cobble', 'stairs:stair_inner_mossycobble',
		'stairs:stair_inner_acacia_wood', 'stairs:stair_inner_aspen_wood',
		'stairs:stair_inner_brick', 'stairs:stair_inner_bronzeblock',
		'stairs:stair_inner_copperblock', 'stairs:stair_inner_desert_cobble',
		'stairs:stair_inner_desert_cobble',
		'stairs:stair_inner_desert_sandstone',
		'stairs:stair_inner_desert_sandstone_block',
		'stairs:stair_inner_desert_sandstone_brick',
		'stairs:stair_inner_desert_stone',
		'stairs:stair_inner_desert_stone_block',
		'stairs:stair_inner_desert_stonebrick', 'stairs:stair_inner_goldblock',
		'stairs:stair_inner_ice', 'stairs:stair_inner_obsidian',
		'stairs:stair_inner_obsidian_block', 'stairs:stair_inner_obsidianbrick',
		'stairs:stair_inner_pine_wood', 'stairs:stair_inner_sandstone',
		'stairs:stair_inner_sandstone_block',
		'stairs:stair_inner_sandstonebrick',
		'stairs:stair_inner_silver_sandstone',
		'stairs:stair_inner_silver_sandstone_block',
		'stairs:stair_inner_silver_sandstone_brick',
		'stairs:stair_inner_snowblock', 'stairs:stair_inner_steelblock',
		'stairs:stair_inner_stone', 'stairs:stair_inner_stone_block',
		'stairs:stair_inner_stonebrick', 'stairs:stair_inner_straw',
		'stairs:stair_inner_tinblock', 'stairs:stair_inner_wood',
		'stairs:stair_inner_glass', 'stairs:stair_inner_junglewood',
		'stairs:stair_inner_obsidian_glass', 'stairs:stair_inner_tinblock'
	}

	local t_nodesSlabs = {
		'stairs:slab_cobble', 'stairs:slab_desert_sandstone',
		'stairs:slab_desert_sandstone_block',
		'stairs:slab_desert_sandstone_brick', 'stairs:slab_mossycobble',
		'stairs:slab_acacia_wood', 'stairs:slab_aspen_wood',
		'stairs:slab_brick', 'stairs:slab_bronzeblock',
		'stairs:slab_copperblock', 'stairs:slab_desert_cobble',
		'stairs:slab_desert_stone', 'stairs:slab_desert_stone_block',
		'stairs:slab_desert_stonebrick', 'stairs:slab_goldblock',
		'stairs:slab_ice', 'stairs:slab_obsidian', 'stairs:slab_obsidian_block',
		'stairs:slab_obsidianbrick', 'stairs:slab_pine_wood',
		'stairs:slab_sandstone', 'stairs:slab_sandstone_block',
		'stairs:slab_sandstonebrick', 'stairs:slab_silver_sandstone',
		'stairs:slab_silver_sandstone_block',
		'stairs:slab_silver_sandstone_brick', 'stairs:slab_snowblock',
		'stairs:slab_steelblock', 'stairs:slab_stone',
		'stairs:slab_stone_block', 'stairs:slab_stonebrick',
		'stairs:slab_straw', 'stairs:slab_wood', 'stairs:slab_glass',
		'stairs:slab_junglewood', 'stairs:slab_obsidian_glass',
		'stairs:slab_tinblock'
	}

	for i_element = 1, #t_nodesStairsNormal do
		smaller_steps.pr_NodeOverrider(t_nodesStairsNormal[i_element], 'normal')
	end

	for i_element = 1, #t_nodesStairsOuter do
		smaller_steps.pr_NodeOverrider(t_nodesStairsOuter[i_element], 'outer')
	end

	for i_element = 1, #t_nodesStairsInner do
		smaller_steps.pr_NodeOverrider(t_nodesStairsInner[i_element], 'inner')
	end

	for i_element = 1, #t_nodesSlabs do
		smaller_steps.pr_NodeOverrider(t_nodesSlabs[i_element], 'slab')
	end
end


--
-- Main body
--

pr_StairsOverriders()
