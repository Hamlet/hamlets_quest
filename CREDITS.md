## HAMLET'S QUEST CREDITS


### BASED ON

[Minetest Game v5.3.0](https://forum.minetest.net/viewtopic.php?t=9724)  
By [Various Contributors](https://github.com/minetest/minetest_game/graphs/contributors)  
Released under [these licenses](https://github.com/minetest/minetest_game/blob/master/LICENSE.txt).


### USER INTERFACE RELATED MODS

[Crafting Guide Plus](https://github.com/random-geek/cg_plus)  
By [random_geek](https://forum.minetest.net/memberlist.php?mode=viewprofile&un=random_geek)  
Released under the [LGPL v3.0](https://en.wikipedia.org/wiki/LGPL) license.

[Help](https://forum.minetest.net/viewtopic.php?t=15912)  
By [Wuzzy](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3082)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[HUD Bars](https://forum.minetest.net/viewtopic.php?t=11153)  
By [Wuzzy](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3082)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Minetest Game item help](https://forum.minetest.net/viewtopic.php?t=15224)  
By [Wuzzy](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3082)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Simple Fast Inventory Buttons](https://forum.minetest.net/viewtopic.php?t=16079)  
By [Wuzzy](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3082)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).


### MAP (WORLD) RELATED MODS

[Basic Materials](https://forum.minetest.net/viewtopic.php?t=21000)  
By [VanessaE](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=1580)  
Released under the [LGPL v3.0](https://en.wikipedia.org/wiki/LGPL) license.

[Charcoal Lump](https://forum.minetest.net/viewtopic.php?t=3971)  
By [torusJKL](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3747)  
Released under the [GPL v2](https://en.wikipedia.org/wiki/Gpl_v2) license or later.

[Dwarf Fortress style caverns](https://forum.minetest.net/viewtopic.php?t=17096)  
By [FaceDeer](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18410)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Moon Phases](https://forum.minetest.net/viewtopic.php?t=24536)  
By [TestificateMods](https://forum.minetest.net/memberlist.php?mode=viewprofile&un=TestificateMods)  
Released under the [LGPL v3.0](https://en.wikipedia.org/wiki/LGPL) license.

[Real Minerals](https://github.com/FaceDeer/real_minerals) ported from [Real Test](https://forum.minetest.net/viewtopic.php?t=2671)  
By [FaceDeer](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18410)  
Released under the [GPL v3](https://en.wikipedia.org/wiki/Gpl_v2) license.

[Round Tree Trunks](https://forum.minetest.net/viewtopic.php?t=20036)  
By [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662)  
Released under the [EUPL v1.2](https://en.wikipedia.org/wiki/European_Union_Public_Licence) license or later.

[Skylayer](https://forum.minetest.net/viewtopic.php?t=15733)  
By [xeranas](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19052)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Snow rain clouds (with sound)](https://forum.minetest.net/viewtopic.php?t=6854)  
By [paramat](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3380)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Stonebrick Dungeons](https://forum.minetest.net/viewtopic.php?t=18457)  
By [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662)  
Released under the [EUPL v1.2](https://en.wikipedia.org/wiki/European_Union_Public_Licence) license or later.

[Under Sky](https://forum.minetest.net/viewtopic.php?t=16058)  
By [Shara](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19807)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Unified Dyes](https://forum.minetest.net/viewtopic.php?t=2178)  
By [VanessaE](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=1580)  
Released under the [GPL v2.0](https://en.wikipedia.org/wiki/GPL_v2) license or later.


### PHYSICS AND DAMAGE RELATED MODS

[Fallen Nodes](https://forum.minetest.net/viewtopic.php?t=18239)  
By [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662)  
Released under the [EUPL v1.2](https://en.wikipedia.org/wiki/European_Union_Public_Licence) license or later.

[Hard Trees Redo](https://forum.minetest.net/viewtopic.php?f=11&t=20005)  
By [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662)  
Released under the [EUPL v1.2](https://en.wikipedia.org/wiki/European_Union_Public_Licence) license or later.

[Radiant Damage](https://forum.minetest.net/viewtopic.php?t=20185)  
By [FaceDeer](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18410)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Soft Leaves](https://forum.minetest.net/viewtopic.php?t=20141)  
By [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662)  
Released under the [EUPL v1.2](https://en.wikipedia.org/wiki/European_Union_Public_Licence) license or later.


### FLORA (PLANTS) RELATED MODS

[Autoplant](https://github.com/krondor-game/autoplant)  
By [neoascetic](https://github.com/neoascetic)  
Released under the [WTFPL](https://en.wikipedia.org/wiki/WTFPL) license.

[Desert Life](https://forum.minetest.net/viewtopic.php?t=16054)  
By [NathanSalapat](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=11543)  
Released under the [CC BY-SA v4.0](https://en.wikipedia.org/wiki/CC-BY-SA-4.0) license.

[Farming Redo](https://forum.minetest.net/viewtopic.php?t=9019)  
By [TenPlus1](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=6871)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Real Trees](https://forum.minetest.net/viewtopic.php?t=16286)  
By [yzelast](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19987)  
Released under the [WTFPL](https://en.wikipedia.org/wiki/WTFPL) license.


### CHARACTER RELATED MODS

[3D Armor](https://forum.minetest.net/viewtopic.php?t=4654)  
By [stu](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=4188)  
Released under the [LGPL v2.1](https://en.wikipedia.org/wiki/LGPL) license.

[Armor HUD bar](https://forum.minetest.net/viewtopic.php?t=11337)  
By [Wuzzy](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3082)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Extra armors for 3d_armor](https://forum.minetest.net/viewtopic.php?t=16645)  
By [davidthecreator](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=11158)  
Released under the [CC BY-SA v3.0](https://en.wikipedia.org/wiki/CC-BY-SA-3.0) license.

[Hunger Next Generation](https://forum.minetest.net/viewtopic.php?t=19664)  
By [Linuxdirk](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=11436)  
Released under the [GPL v3.0](https://en.wikipedia.org/wiki/GPL_v3) license.

[Sprint w. hudbars, hunger, monoids support](https://forum.minetest.net/viewtopic.php?t=18069)  
By [texmex](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19662)  
Released under the [LGPL v3.0](https://en.wikipedia.org/wiki/LGPL) license.

[Thirsty](https://forum.minetest.net/viewtopic.php?t=11820)  
By [Ben](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=13425)  
Released under the [LGPL v2.1](https://en.wikipedia.org/wiki/LGPL) license.


### TOOLS AND ALIKE RELATED MODS

[Anvil](https://github.com/minetest-mods/anvil)  
By [Sokomine](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=2936)  
Released under the [GPL v3.0](https://en.wikipedia.org/wiki/GPL_v3) license.

[Archer](https://forum.minetest.net/viewtopic.php?t=17007)  
By [Saavedra29](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=20750)  
Released under the [WTFPL](https://en.wikipedia.org/wiki/WTFPL) license.

[Backpacks](https://forum.minetest.net/viewtopic.php?t=14579)  
By [everamzah](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=12651)  
Released under the [GPL v3.0](https://en.wikipedia.org/wiki/GPL_v3) license.

[Castle Weapons](https://github.com/minetest-mods/castle_weapons)  
By [philipbenr](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=6103) and [Dan DunCombe](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=5529)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Enchanting](https://forum.minetest.net/viewtopic.php?t=14087)  
By [jp](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=8608) and [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662)  
Released under the [GPL v3.0](https://en.wikipedia.org/wiki/GPL_v3) license.

[Fort Spikes](https://forum.minetest.net/viewtopic.php?t=14574)  
By [xeranas](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19052)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Gravel Sieve](https://forum.minetest.net/viewtopic.php?t=17893)  
By [joe7575](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21063)  
Released under the [LGPL v2.1](https://en.wikipedia.org/wiki/LGPL) license or later.

[Hardcore torchs](https://forum.minetest.net/viewtopic.php?t=18376)  
By [BrunoMine](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=5293)  
Released under the [LGPL v3.0](https://en.wikipedia.org/wiki/LGPL) license.

[Mountain Climbing](https://forum.minetest.net/viewtopic.php?t=17069)  
By [Shara](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19807)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[North Compass](https://github.com/FaceDeer/north_compass)  
By [FaceDeer](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18410)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Path marker signs](https://forum.minetest.net/viewtopic.php?t=16511)  
By [FaceDeer](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18410)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Placeable Books](https://forum.minetest.net/viewtopic.php?t=15214)  
By [everamzah](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=12651)  
Released under the [LGPL v2.1](https://en.wikipedia.org/wiki/LGPL) license or later.

[Re-Cycle Age](https://forum.minetest.net/viewtopic.php?t=23414)  
By [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662)  
Released under the [EUPL v1.2](https://en.wikipedia.org/wiki/European_Union_Public_Licence) license or later.

[Ropes and rope ladders](https://forum.minetest.net/viewtopic.php?t=16559)  
By [FaceDeer](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18410)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Sounding Line](https://forum.minetest.net/viewtopic.php?t=16601)  
By [FaceDeer](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18410)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Unified Hammers](https://forum.minetest.net/viewtopic.php?t=22720)  
By [Linuxdirk](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=11436)  
Released under the [GPL v3.0](https://en.wikipedia.org/wiki/GPL_v3) license.

[Wooden Bucket](https://forum.minetest.net/viewtopic.php?t=16472)  
By [duane](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18344)  
Released under the [LGPL v2.0](https://en.wikipedia.org/wiki/LGPL) license.


### BUILDINGS AND SIMILAR RELATED MODS

[All Walls](https://forum.minetest.net/viewtopic.php?t=18172)  
By [v-rob](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19201)  
Released under the [WTFPL](https://en.wikipedia.org/wiki/WTFPL) license.

[Bunkbed](https://forum.minetest.net/viewtopic.php?t=23684)  
By [Nordal](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=23411)  
Released under the [LGPL v2.1](https://en.wikipedia.org/wiki/LGPL) license or later.

[Campfires](https://forum.minetest.net/viewtopic.php?t=3794)  
By [Doc](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3443)  
Modified by [Napiophelios] (https://github.com/Napiophelios/campfire)  
Released under the [WTFPL](https://en.wikipedia.org/wiki/WTFPL) license.

[Carpet](https://forum.minetest.net/viewtopic.php?t=2875)  
By [Jordach](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=332)  
Released under the [LGPL](https://en.wikipedia.org/wiki/LGPL) license.

[Castle Doors - Cottage Doors - Door Wood](https://forum.minetest.net/viewtopic.php?t=10626)  
By [Don](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=10447)  
Released under the [DWYWPL](https://forum.minetest.net/viewtopic.php?t=14246) license.

[Castle Gates](https://github.com/minetest-mods/castle_gates)  
By [philipbenr](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=6103) and [Dan DunCombe](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=5529)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Castle Lighting](https://github.com/minetest-mods/castle_lighting)  
By [philipbenr](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=6103) and [Dan DunCombe](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=5529)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Castle Masonry](https://github.com/minetest-mods/castle_masonry)  
By [philipbenr](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=6103) and [Dan DunCombe](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=5529)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Castle Shields](https://github.com/minetest-mods/castle_shields)  
By [philipbenr](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=6103) and [Dan DunCombe](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=5529)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Castle Storage](https://github.com/minetest-mods/castle_storage)  
By [philipbenr](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=6103) and [Dan DunCombe](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=5529)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Castle Tapestries](https://github.com/minetest-mods/castle_tapestries)  
By [philipbenr](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=6103) and [Dan DunCombe](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=5529)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Darkage](https://forum.minetest.net/viewtopic.php?f=11&t=3213&start=125#p253141)  
[Addi](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=3040)'s fork  
Released under the [WTFPL](https://en.wikipedia.org/wiki/WTFPL) license.

[Frame](https://forum.minetest.net/viewtopic.php?t=16585)  
By [sofar](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=12512)  
Released under the [LGPL v2.1](https://en.wikipedia.org/wiki/LGPL) license or later.

[Hidden Doors](https://forum.minetest.net/viewtopic.php?t=18294)  
By [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662) and [Napiophelios](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=10856)  
Released under the [EUPL v1.2](https://en.wikipedia.org/wiki/European_Union_Public_Licence) license or later.

[Library](https://forum.minetest.net/viewtopic.php?t=18359)  
By [v-rob](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19201)  
Released under the [MIT License](https://en.wikipedia.org/wiki/MIT_License).

[Placeable Ingots](https://forum.minetest.net/viewtopic.php?t=21491)  
By [Skamiz Kazzarch](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=22843)  
Released under the [LGPL v2.1](https://en.wikipedia.org/wiki/LGPL) license.

[Simple Arcs](https://forum.minetest.net/viewtopic.php?t=22839)  
By [TumeniNodes](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=19111)  
Released under the [LGPL v2.1](https://en.wikipedia.org/wiki/LGPL) license or later.

[Smaller Steps](https://forum.minetest.net/viewtopic.php?t=18283)  
By [Hamlet](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=21662)  
Released under the [GPL v3.0](https://en.wikipedia.org/wiki/GPL_v3) license.

[TS Furniture](https://forum.minetest.net/viewtopic.php?t=14302)  
By [Thomas-S](https://forum.minetest.net/memberlist.php?mode=viewprofile&u=18458)  
Released under the [CC0](https://en.wikipedia.org/wiki/Creative_Commons_license#Zero_/_public_domain) license.

